import { Component } from '@angular/core';
import { Router }  from '@angular/router';
import { AngularFire, AuthProviders } from 'angularfire2';
import { FacebookService, FacebookInitParams, FacebookLoginStatus } from 'ng2-facebook-sdk';
import { AuthService } from './services/auth.service';
import { FbBackendService } from './services/fb-backend.service';
import { Profile } from './model/profile';
import { MockBackend } from '@angular/http/testing';
import { environment } from './../environments/environment';
import { Response, ResponseOptions } from '@angular/http';
import 'rxjs/add/observable/throw';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  private db: Profile = new Profile("Agustin Sarasua");
  title = 'app works!';
  user = {};
  constructor (private backend: MockBackend, private fb: FacebookService, public af: AngularFire, private router: Router, public authService: AuthService, public fbBackendService: FbBackendService) {
    let fbParams: FacebookInitParams = {
                                   appId: '1709613959262846',
                                   xfbml: true,
                                   version: 'v2.7'
                                   };
    this.fb.init(fbParams);
    this.backend.connections.subscribe( c => {

      let singleTicketMatcher = /\/api\/ticket\/([0-9]+)/i;        
      
      // return all tickets
      // GET: /ticket
      if (c.request.url === environment.coreBackendUrl + 'profile' && c.request.method === 0) {
        let res = new Response(new ResponseOptions({
          body: JSON.stringify(this.db),
          status: 200
        }));
        
        c.mockRespond(res);
      }
      // // return ticket matching the given id
      // // GET: /ticket/:id
      // else if (c.request.url.match(singleTicketMatcher) && c.request.method === 0) {
      //   let matches = this.db.filter( (t) => {
      //     return t._id == c.request.url.match(singleTicketMatcher)[1]
      //   });
        
      //   c.mockRespond(new Response({
      //     body: JSON.stringify(matches[0])
      //   })); 
      // }
      // // Add or update a ticket
      // // POST: /ticket
      // else if (c.request.url === 'http://localhost:8080/api/ticket' && c.request.method === 1) {
      //   let newTicket: Ticket = JSON.parse(c.request._body);
        
      //   let existingTicket = this.db.filter( (ticket: Ticket) => { return ticket._id == newTicket._id});
      //   if (existingTicket && existingTicket.length === 1) {
      //     Object.assign(existingTicket[0], newTicket);
          
      //     c.mockRespond(new Response({
      //       body: JSON.stringify(existingTicket[0])
      //     }));
      //   } else {
      //     newTicket._id = parseInt(_.max(this.db, function(t) {
      //       return t._id;
      //     })._id || 0, 10) + 1 + '';
    
      //     this.db.push(newTicket);
          
      //     c.mockRespond(new Response({
      //       body: JSON.stringify(newTicket)
      //     }));
      //   }
      // }
      // // Delete a ticket
      // // DELETE: /ticket/:id
      // else if (c.request.url.match(singleTicketMatcher) && c.request.method === 3) {
      //   let ticketId = c.request.url.match(singleTicketMatcher)[1];
      //   let pos = _.indexOf(_.pluck(this.db, '_id'), ticketId);
        
      //   this.db.splice(pos, 1);
        
      //   c.mockRespond(new Response({
      //     body: JSON.stringify({})
      //   }));
      // }
      
    });

    this.af.auth.subscribe(user => this.authenPimba(user));
  }

  authenPimba(user):any{
    if(user != null) {
      console.log(user);
      this.authService.login();
      localStorage.setItem("pimbaUserId", user.uid);
      localStorage.setItem("pimbaToken", user.auth.Ed);
      this.fb.getLoginStatus().then(
        (response: FacebookLoginStatus) => {
          console.log(response);
          this.fbBackendService.indexToken(response.authResponse.accessToken, response.authResponse.userID);
          this.router.navigate(['/home']);
        },
        (error: any) => console.error(error)
      );
    }else{
      this.router.navigate(['/login']);
    }
  }

}
