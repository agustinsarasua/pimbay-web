import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from "rxjs/Observable";
import { RestService } from "./rest.service";
import { Event } from "./../model/event"
import { CollectionResponse } from "./../model/collection-response"
import { environment } from './../../environments/environment';

@Injectable()
export class EventService {

  constructor(private http: Http, private restService: RestService) { }

  public loadUserEvents():Promise<CollectionResponse<Event>> {
    let headers = new Headers({ 'Content-Type': 'application/json' });

    return this.restService.doGet<CollectionResponse<Event>>(environment.fbBackendUrl, "events", headers, true);
  }

  public loadEventsRecomended():Promise<CollectionResponse<Event>> {
    let headers = new Headers({ 'Content-Type': 'application/json' });

    return this.restService.doGet<CollectionResponse<Event>>(environment.fbBackendUrl, "events/recommended", headers, true);
  }
}
