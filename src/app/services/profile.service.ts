import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from "rxjs/Observable";
import { RestService } from "./rest.service";
import { Event } from "./../model/event"
import { Profile } from "./../model/profile"
import { environment } from './../../environments/environment';

@Injectable()
export class ProfileService {

  constructor(private http: Http, private restService: RestService) { }

  public loadUserProfile():Promise<Profile> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.restService.doGet<Profile>(environment.coreBackendUrl, "profile", headers, true);
  }

}
