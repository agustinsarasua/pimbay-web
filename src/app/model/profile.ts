import {Location} from './location';
import {Cover} from './cover';

export class Profile {

    public id: string;
    public name: string;
    public lastName: string;
    public birthday: string;
    public email: string;

    constructor(name: string){
        this.name = name;
    }
}