import {Location} from './location';
import {Cover} from './cover';

export class Event {

    public id: number;
    public name: string;
    public description: string;
    public location: Location;
    public startTime: Date;
    public endTime: Date;
    public src: string;  

}

export enum EventViewType {
    COMMON,
    IMPORTANT,
    RECOMMENDED
}