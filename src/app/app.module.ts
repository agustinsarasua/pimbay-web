import { AngularFireModule, AuthMethods, AuthProviders } from 'angularfire2';
import { MockBackend } from '@angular/http/testing';
import { environment } from '../environments/environment';
import { MaterialModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http, BaseRequestOptions } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import 'hammerjs';
import { AppComponent } from './app.component';
import { HeaderComponent } from './common/header/header.component';
import { HomePageComponent } from './pages/home/home.component';
import { AuthService } from './services/auth.service'
import { FbBackendService } from './services/fb-backend.service';
import { EventService } from './services/event.service';
import { ProfileService } from './services/profile.service';
import { RestService } from './services/rest.service';
import { AuthGuard } from './auth.guard';
import { EventCardComponent } from './common/events/event-card/event-card.component';
import { LoginComponent } from './pages/login/login.component';
import { enableDebugTools } from '@angular/platform-browser';
import { FacebookService } from 'ng2-facebook-sdk';

const appRoutes: Routes = [
  { path: 'home', component: HomePageComponent, canActivate: [AuthGuard]},
  { path: 'login', component: LoginComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomePageComponent,
    EventCardComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    FlexLayoutModule.forRoot(),
    MaterialModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase, {
      provider: AuthProviders.Facebook,
      method: AuthMethods.Popup,
      scope: ['user_likes']
    })
  ],
  providers: [
    BaseRequestOptions, 
    MockBackend, 
    {
       provide: Http,
       deps: [MockBackend, BaseRequestOptions],
       useFactory: (backend, options) => { return new Http(backend, options); }
     },
    FacebookService, 
    RestService, 
    FbBackendService, 
    AuthService, 
    AuthGuard, 
    EventService,
    ProfileService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
