import { Component, OnInit } from '@angular/core';
import { Event, EventViewType } from './../../model/event';
import { Cover } from './../../model/cover';
import { EventService } from './../../services/event.service';
import { ProfileService } from './../../services/profile.service';
import 'rxjs/add/observable/throw';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomePageComponent implements OnInit {

  facebookEvents: Array<Event> = [];

  eventsRecommended: Array<Event> = [];

  EventViewType : typeof EventViewType = EventViewType;

  constructor(private eventService: EventService, private profileService: ProfileService) {
    
   }

  ngOnInit() {
    this.profileService.loadUserProfile().then(profile => {
      console.log(profile);
    });

    this.eventService.loadEventsRecomended().then(collectionResponse =>
      {
        console.log(collectionResponse);
        this.eventsRecommended = collectionResponse.items;
      });

    this.eventService.loadUserEvents().then(collectionResponse =>
      {
        console.log(collectionResponse);
        this.facebookEvents = collectionResponse.items;
      });
  }

}
