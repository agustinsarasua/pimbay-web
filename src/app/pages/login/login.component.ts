import { Component, OnInit } from '@angular/core';
import { Router }  from '@angular/router';
import { AngularFire, AuthProviders } from 'angularfire2';
import { AuthService } from './../../services/auth.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor (public af: AngularFire, private router: Router, public authService: AuthService) {
    this.af.auth.subscribe(user => this.authenPimba(user));
  }

  ngOnInit() {
    //called after the constructor and called  after the first ngOnChanges() 
  }

  authenPimba(user):any {
    if(user != null) {
      localStorage.setItem("pimbaUserId", user.uid);
      localStorage.setItem("pimbaToken", user.auth.Ed);
      //this.authService.login();
      this.router.navigate(['/home']);
    }else{
      this.router.navigate(['/login']);
    }
  }

  login(){
    this.af.auth.login().then(data => {
      console.log(data);
      this.authenPimba(data);
    }, error => {
      console.log(error);
    });
  }

}
