import { Component, OnInit, Input, Output } from '@angular/core';
import { Event, EventViewType } from './../../../model/event'

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.scss']
})
export class EventCardComponent implements OnInit {

  @Input('event') event: Event;

  @Input('viewType') viewType: EventViewType;

  EventViewType : typeof EventViewType = EventViewType;

  constructor() { }

  ngOnInit() {
  }

}
