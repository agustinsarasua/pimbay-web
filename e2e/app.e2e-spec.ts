import { PimbayWebPage } from './app.po';

describe('pimbay-web App', () => {
  let page: PimbayWebPage;

  beforeEach(() => {
    page = new PimbayWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
